import React, { useState, useEffect } from 'react';
import initData from './product.json';
import Card from '../../components/elements/Card/Card';
import SearchBar from '../../components/elements/SearchBar/SearchBar';
import sortIco from '../../assets/sort.svg';

export default function Products() {
  const [data, setData] = useState(initData);
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState(0);

  const handleSort = () => {
    setSort(sort === 0 ? 1 : 0);
  };

  useEffect(() => {
    const filteredData = initData.filter((i) =>
      i.name.toLowerCase().includes(search.toLowerCase())
    );
    if (sort === 1) {
      setData(filteredData.sort((a, b) => b.price - a.price));
    } else {
      setData(filteredData.sort((a, b) => a.price - b.price));
    }
  }, [search, sort]);

  // console.log(search);
  // const handleSearch = (e) => {
  //   setSearch(e.target.value);
  //   const filteredData = initData.filter((item) =>
  //     item.name.includes(e.target.value)
  //   );
  //   setData(filteredData);
  // };

  return (
    <div>
      <div className="max-w-xs mx-auto h-1/2 md:max-w-sm">
        <SearchBar setSearch={setSearch} />
        <div className="flex justify-between md:justify-center items-center gap-x-6 mt-8 mx-5">
          <div className="flex flex-col justify-center items-start md:items-center">
            <h1 className="font-bold text-2xl md:text-4xl">Products</h1>
            <p className="text-sm md:text-lg">{`${data.length} products found`}</p>
          </div>
          <img
            className="w-6 h-6 cursor-pointer"
            onClick={handleSort}
            src={sortIco}
          />
        </div>
      </div>
      <div className="flex justify-center">
        <div className="grid grid-cols-2 md:grid-cols-4 gap-4 my-8 max-w-max">
          <Card items={data} />
        </div>
      </div>
    </div>
  );
}
