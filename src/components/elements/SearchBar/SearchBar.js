import React from 'react';
import searchIco from '../../../assets/search.svg';

export default function SearchBar({ setSearch }) {
  return (
    <form className="mt-12 mb-8">
      <label className="relative block">
        <img
          className="absolute top-[50%] translate-y-[-50%] w-4 h-4 left-3"
          src={searchIco}
        ></img>
        <span className="sr-only">Search</span>
        <input
          type="text"
          name="search"
          className="block bg-white w-full text-sm rounded-md py-2 pl-9 pr-3 placeholder:text-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
          placeholder="Search product"
          onChange={(e) => setSearch(e.target.value)}
        />
      </label>
    </form>
  );
}
