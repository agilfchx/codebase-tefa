import React from 'react';
import PropTypes from 'prop-types';

export default function Card({ items }) {
  const dotNumber = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };
  return (
    <>
      {items.map((i, idx) => (
        <div
          key={idx}
          className="bg-white max-w-[220px] min-h-[400px] rounded-xl overflow-hidden p-3 relative md:min-h-[320px]"
        >
          <div className="flex justify-center">
            <img
              className="w-full md:h-32 rounded-md object-cover"
              src={i.image}
            />
          </div>
          <div className="mt-4">
            <h1 className="font-medium p-2 text-xs md:text-lg">{i.name}</h1>
            <p className="text-[#7C7A7A] truncate p-2 text-xs md:text-lg">
              {i.description}
            </p>
          </div>
          <div className="absolute bottom-2">
            <p className="font-medium p-2 text-xs md:text-lg">
              Rp {dotNumber(i.price)}
            </p>
          </div>
        </div>
      ))}
    </>
  );
}

Card.propTypes = {
  items: PropTypes.array.isRequired,
};
